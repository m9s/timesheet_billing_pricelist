# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Timesheet Billing Pricelist',
    'name_de_DE': 'Zeiterfassung Abrechnung Preisliste',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Integrates billing pricelists on timesheets
    ''',
    'description_de_DE': '''
    - Integriert Preislisten für Abrechnungsdaten mit Zeiterfassungsblättern
    ''',
    'depends': [
        'account_invoice_billing_pricelist',
        'timesheet_billing',
    ],
    'xml': [
    ],
    'translation': [
    ],
}
